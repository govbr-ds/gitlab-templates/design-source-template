const { branches, changelog, commitAnalyzer, git, gitlab, releaseNotesGenerator } = require('@govbr-ds/release-config')

module.exports = {
  branches: branches,
  plugins: [commitAnalyzer, releaseNotesGenerator, changelog, git, gitlab],
}
